package com.spring.jersey.demo.util;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component // register JerseyConfig - JAX-RS Application class to be managed by Spring
public class JerseyConfig extends ResourceConfig {
	
	public JerseyConfig() {
		// register the JAX-RS components with Jersey
		packages("com.spring.jersey.demo.rest");
	}
}
