package com.spring.jersey.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.spring.jersey.demo.model.Employee;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {

}
