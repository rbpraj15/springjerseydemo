package com.spring.jersey.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.jersey.demo.model.Employee;
import com.spring.jersey.demo.repo.EmployeeRepo;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepo employeeRepo;
	
	/**
	 * Retrieves <code>Employee</code> from repository.
	 * @param id
	 * @return <code>Employee</code>
	 */
	@Transactional(readOnly=true)
	public Employee get(Long id){
		return this.employeeRepo.findOne(id);
	}
	
	/**
	 * Saves <code>Employee</code> in repository.
	 */
	@Transactional
	public Employee save(Employee employee){
		return this.employeeRepo.save(employee);
	}
	
	/**
	 * Retrieves <code>Iterable<Employee></code> from repository.
	 * @param id
	 * @return <code>Employee</code>
	 */
	@Transactional(readOnly=true)
	public Iterable<Employee> getAll(){
		return this.employeeRepo.findAll();
	}
	
	/**
	 * Deletes the <code>Employee</code> from repository.
	 * @param employee
	 */
	@Transactional
	public void delete(Employee employee){
		this.employeeRepo.delete(employee);
	}
}
