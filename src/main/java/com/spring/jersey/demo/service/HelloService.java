package com.spring.jersey.demo.service;

import org.springframework.stereotype.Service;

@Service
public class HelloService {
	
	public String sayHello(String name){
		String message = String.format("Hello %s! Welcome to Spring-Jersey Demo", name);
		return message;
	}
}
