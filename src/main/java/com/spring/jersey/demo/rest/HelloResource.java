package com.spring.jersey.demo.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.spring.jersey.demo.service.HelloService;

@Path("hello")
public class HelloResource {

	private static Logger logger = LoggerFactory.getLogger(HelloResource.class);
	
	@Autowired
	private HelloService helloService;
	
	@GET
	@Path("{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getHello(@PathParam("name") String name){
		logger.info("Greeting to {}", name);
		return this.helloService.sayHello(name);
	}
}
