package com.spring.jersey.demo.rest;

import java.net.URI;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;

import com.spring.jersey.demo.model.Employee;
import com.spring.jersey.demo.service.EmployeeService;

@Path("employees")
public class EmployeeResource {

	@Autowired
	private EmployeeService employeeService;
	
	@Context
	private UriInfo uriinfo;
	
	/**
	 * Retrieves a list of <code>Employee</code>
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Iterable<Employee> get(){
		return employeeService.getAll();
	}
	
	/**
	 * Retrieves <code>Employee</code>
	 * @param id
	 * @return
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Employee get(@PathParam("id") Long id){
		return employeeService.get(id);
	}
	
	/**
	 * Creates <code>Employee</code>
	 * @param employee
	 * @return
	 */
	@POST
	public Response create(Employee employee){
		Employee saved = this.employeeService.save(employee);
		URI location = uriinfo.getAbsolutePathBuilder().path("/"+saved.getId()).build();
		return Response.created(location).build();
	}
	
	/**
	 * Updates <code>Employee</code>
	 * @param employee
	 * @return
	 */
	@PUT
	@Path("/{id}")
	public Response update(@PathParam("id") Long id, Employee employee){
		employee.setId(id);
		this.employeeService.save(employee);
		return Response.noContent().build();
	}
	
	/**
	 * Deletes <code>Employee</code>
	 * @param employee
	 * @return
	 */
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id){
		Employee employee = this.employeeService.get(id);
		if(employee == null){
			return Response.status(Status.NOT_FOUND).build();
		}else{
			this.employeeService.delete(employee);
			return Response.ok().build();
		}
	}
}
